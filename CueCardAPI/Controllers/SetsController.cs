﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CueCardAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace CueCardAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SetsController : ControllerBase
    {
        // GET api/set/user={userID}
        [HttpGet("user={userID}")]
        public ActionResult<Set[]> GetQuery(string userID)
        {

            Set[] sets = new Set[3];
            sets[0] = new Set("Science 1", "hui1123ygu");
            sets[1] = new Set("English 1", "sdfasdfada");
            sets[2] = new Set("Software Engineering 1", "435refdadsad");
            return sets;
           
        }


        // GET api/set/ user={groupID}
        [HttpGet("group={groupID}")]
        public ActionResult<Set[]> Get(string groupID)
        {

            Set[] sets = new Set[3];
            sets[0] = new Set("Math 1", "hui1123ygu");
            sets[1] = new Set("Math 2", "sdfasdfada");
            sets[2] = new Set("Math 3", "435refdadsad");
            return sets;

        }
    }
}


