﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using CueCardAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace CueCardAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class GroupsController : ControllerBase
	{

		static HttpClient client = new HttpClient();

		// GET api/group/user={userID}
		[HttpGet("user={userID}")]
		public async Task<ActionResult<Group[]>> GetQueryAsync(string userID)
		{

			Group[] groups = await GetAsyncTest();

			//Group[] groups = new Group[5];
			//groups[0] = new Group("Math", "1");
			//groups[1] = new Group("Computer Science", "2");
			//groups[2] = new Group("Science", "3");
			//groups[3] = new Group("Art", "4");
			//groups[4] = new Group("English", "5");
			return groups;

		}

		public async Task<Group[]> GetAsyncTest()
		{
			var response = await client.GetAsync("https://firestore.googleapis.com/v1/projects/cue-card-conundrum/databases/(default)/documents/Groups/");

			if (response.IsSuccessStatusCode)
			{
				Console.WriteLine("------------------------------------------------------- BIG GAY --------------------------------------------------------");
				string test = await response.Content.ReadAsStringAsync();
				//Console.WriteLine(test);

				JObject json = JObject.Parse(test);
				var fields = json["documents"][0]["fields"];
				string name = fields["name"]["stringValue"].ToString();
				var ownerID = fields["owner"]["referenceValue"];
				User owner = new Student();
				var memberIDs = fields["users"];
				User[] members = { new Student(), new Student(), new Student() };
				var setIDs = fields["sets"]["arrayValue"]["values"];
				Set[] sets = { new Set("Science 1", "hui1123ygu"), new Set("English 1", "sdfasdfada"), new Set("Software Engineering 1", "435refdadsad") };

				Group group = new Group(name, owner, members, sets);
				Group[] groups = { group };
				return groups;
			}
			else
			{
				Group[] groups = { new Group("test", "test")};
				return groups;
			}

		}



	}


}