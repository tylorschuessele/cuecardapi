﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CueCardAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CueCardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardsController : ControllerBase
    {

        // GET api/cards/set={setID}
        [HttpGet("set={setID}")]
        public ActionResult<Card[]> Get(string setID)
        {
            Card[] cards = new Card[6];
            cards[0] = new CueCard("Question 1", "Answer 1");
            cards[1] = new CueCard("Question 2", "Answer 2");
            cards[2] = new CueCard("Question 3", "Answer 3");
            cards[3] = new CueCard("Question 4", "Answer 4");
            cards[4] = new CueCard("Question 5", "Answer 5");
            cards[5] = new CueCard("Question 6", "Answer 6");
            return cards; 
        }
    }
}