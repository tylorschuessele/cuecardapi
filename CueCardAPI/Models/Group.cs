﻿using System;
namespace CueCardAPI.Models
{
    public class Group
    {
        protected string _name;
        protected string _id;
        protected Set[] _sets;
        protected User[] _members;
        protected User _owner;

        public Group(string name, string id)
        {
            _name = name;
            _id = id;
        }

        public Group(string n, User o)
        {
            _name = n;
            _owner = o;

        }

        public Group(string n, User o, Set[] s)
        {
            _name = n;
            _owner = o;
            _sets = s;
        }

        public Group(string n, User o, User[] m)
        {
            _name = n;
            _owner = o;
            _members = m;

        }

        public Group(string n, User o, User[] m, Set[] s)
        {
            _name = n;
            _owner = o;
            _members = m;
            _sets = s;
        }

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string id
        {
            get { return _id; }
            set { _id = value; }
        }

        public void addSet(Set s)
        {
            Set[] newSets = new Set[(_sets.Length + 1)];
            newSets[0] = s;
            for (int i = 1; i < (_sets.Length); i++)
            {
                newSets[i] = _sets[i];
            }
            _sets = newSets;
        }


    }
}
