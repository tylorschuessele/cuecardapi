﻿using System;
namespace CueCardAPI.Models
{
    public class CueCard : Card
    {
        
        public CueCard(string question, string answer) : base(question, answer)
        {
            this._question = question;
            this._answer = answer;

        }

    }
}
